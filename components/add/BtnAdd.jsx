import { useRouter } from "next/router";
import React from "react";
import { Button } from "react-bootstrap";

const BtnAdd = () => {
  const router = useRouter();
  return (
    <Button
      style={{
        width: "50px",
        height: "50px",
        position: "fixed",
        zIndex: 100,
        bottom: "40px",
        right: "40px",
        backgroundColor: "#41B07B",
      }}
      className="text-center fw-bold fs-4 rounded-circle border-0 shadow btnHover"
      onClick={() => router.push("/add")}
    >
      +
    </Button>
  );
};

export default BtnAdd;
