import { Card } from "react-bootstrap";
import Img from "./Img";
import Menu from "./Menu";

const Game = ({ children, id }) => {
  const cardStyle = {
    backgroundColor: "#DEF1E0",
  };

  return (
    <>
      <Card style={cardStyle} className="my-4 p-0 border-0">
        <Img />
        <Menu id={id} />
        {children}
      </Card>
    </>
  );
};

export default Game;
