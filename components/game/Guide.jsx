import { Card } from "react-bootstrap";

const Guide = ({ guide }) => {
  const cardStyle = { height: "500px" };

  return (
    <>
      <Card.Body style={cardStyle} className="d-grid align-items-center">
        <Card.Title
          style={{ color: "#41B07B" }}
          className="text-center fs-1 fw-bold"
        >
          {guide}
        </Card.Title>
      </Card.Body>
    </>
  );
};

export default Guide;
