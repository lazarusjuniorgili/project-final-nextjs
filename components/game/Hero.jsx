import { Card, Button } from "react-bootstrap";
import Link from "next/link";

const Hero = ({ id }) => {
  const cardStyle = { height: "500px" };
  const bgStyle = {
    width: "100%",
    maxHeight: "500px",
    objectFit: "cover",
    objectPosition: "center",
  };
  const btnStyle = {
    width: "250px",
    backgroundColor: "#65DBA3",
    color: "white",
    letterSpacing: ".1rem",
  };

  return (
    <>
      <Card
        style={cardStyle}
        className="d-grid align-items-center p-0 border-0"
      >
        <Card.Img src="/images/bg-game.jpg" style={bgStyle} variant="bottom" />
        <div className="d-flex justify-content-center align-items-end">
          <Link
            style={btnStyle}
            href={`/game/${id}/start`}
            className="btn shadow text-uppercase position-absolute mb-5 border-0 fs-3 fw-bold btnHover"
          >
            Start Now
          </Link>
        </div>
      </Card>
    </>
  );
};

export default Hero;
