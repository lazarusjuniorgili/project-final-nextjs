import { Card } from "react-bootstrap";
import Link from "next/link";

const Menu = ({ id }) => {
  const menus = ["game", "detail", "guide"];
  const headerStyle = { backgroundColor: "#60BD8F" };
  const menuStyle = { letterSpacing: ".1rem" };

  return (
    <Card.Header
      style={headerStyle}
      className="d-flex justify-content-center text-white border-0 shadow"
    >
      {menus.map((menu, i) => {
        return (
          <Card key={i} className="bg-transparent border-0">
            <Link
              style={menuStyle}
              className="fs-6 mx-3 nav-link"
              href={menu === "game" ? `/game/${id}` : `/game/${id}/${menu}`}
            >
              {menu.toUpperCase()}
            </Link>
          </Card>
        );
      })}
    </Card.Header>
  );
};

export default Menu;
