import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { Container, Navbar, Nav, Button } from 'react-bootstrap';
import { appbar, nav_brand, btn_primary, btn_secondary } from './Landing.module.css';
import { useState, useEffect } from 'react';

const Appbar = () => {
  const router = useRouter();
  const [username, setUsername] = useState("");
  const [token, setToken] = useState("");
  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    const username = window.localStorage.getItem("username");
    const accessToken = window.localStorage.getItem("accessToken");
    setUsername(username);
    setToken(accessToken);
    setInitializing(false);
  }, []);

  if (initializing) return null;

  return (
    <Navbar bg="light" expand="lg" className={appbar}>
      <Container expand="lg">
        <Navbar.Brand className={nav_brand}>
          <Link href="/">
            <Image src="/images/icon.ico" width={50} height={50} alt="icon" />
            .ico
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Nav className="me-auto my-2 my-lg-0" style={{ maxHeight: '100px' }}>
            <Link href="/" className="nav-link">
              Home
            </Link>
            <Link href="/leaderboard" className="nav-link">
              Leaderboard
            </Link>
          </Nav>
          {token ? (
            <Nav className="d-flex">
              <Link href="/profile" className="navbar-nav_profile_icon">
                <img
                  className=" rounded-circle"
                  src={`https://ui-avatars.com/api/?name=${username}&background=random&size=50`}
                  alt="profile avatar"
                ></img>
              </Link>
            </Nav>
          ) : (
            <Nav className="d-flex gap-3">
              <Button
                onClick={() => router.push('/login')}
                className={btn_primary}>
                Login
              </Button>
              <Button
                onClick={() => router.push('/register')}
                className={btn_secondary}>
                Register
              </Button>
            </Nav>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Appbar;
