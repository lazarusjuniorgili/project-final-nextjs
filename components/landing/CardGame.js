import React from 'react';
import { useRouter } from 'next/router';
import { Container, Row, Button } from 'react-bootstrap';
import {
  card_wrapper,
  btn_secondary,
  btn_less,
} from './Landing.module.css';
import CardItem from './CardItem';

const CardGame = ({ games }) => {
  const router = useRouter();

  const itemPerRow = 4;
  const [next, setNext] = React.useState(itemPerRow);

  const handleMoreImage = () => setNext(next + itemPerRow);
  const handleLessImage = () => setNext(next - itemPerRow);

  return (
    <Container className={card_wrapper}>
      <h2 className="text-center mb-4">LIST GAME</h2>
      {games?.length === 0 ? (
        <h3 className="my-5 py-5 text-center">Game not found!</h3>
      ) : (
        <Row xs={1} md={3} lg={4} className="g-4">
          {games?.slice(0, next)?.map((game) => (
            <CardItem key={game?.id} {...game} />
          ))}
        </Row>
      )}

      <div className="d-flex justify-content-center">
        {itemPerRow < next && (
          <Button className={`m-4 ${btn_less}`} onClick={handleLessImage} variant="link">
            show less
          </Button>
        )}
        {next < games?.length && (
          <Button className={`m-4 ${btn_secondary}`} onClick={handleMoreImage}>
            more game
          </Button>
        )}
      </div>
    </Container>
  );
};

export default CardGame;
