import { useRouter } from 'next/router';
import { Col, Card, Button } from 'react-bootstrap';
import {
  card_info,
  card_title,
  btn_play,
} from './Landing.module.css';

const CardItem = ({ id, name, cover }) => {
  const router = useRouter();

  return (
    <Col>
      <Card className="text-center">
        <Card.Img
          variant="top"
          src={cover?.includes('.jpg') ? `images/${cover}` : 'images/cyberpunk.jpg'}
        />
        <Card.Body className={card_info}>
          <Card.Title className={card_title}>{name}</Card.Title>
          <Button className={btn_play} onClick={() => router.push(`/game/${id}`)}>
            PLAY NOW
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default CardItem;
