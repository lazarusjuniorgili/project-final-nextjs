import Link from 'next/link';
import style from './Landing.module.css';

const Footer = () => {
  return (
    <footer className={style['app-footer']}>
      <Link href="/">&copy; 2023 - Game Project Binar Academy</Link>
    </footer>
  );
};

export default Footer;
