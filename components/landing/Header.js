import Head from 'next/head';

const Header = () => {
  return (
    <Head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Game App</title>
      <link rel="icon" href="images/icon.ico" />
    </Head>
  );
};

export default Header;
