import React from 'react';
import { Carousel } from 'react-bootstrap';
import {
  car_wrapper,
  car_item,
  car_caption
} from '../landing/Landing.module.css';

const HeroLanding = ({ games }) => {
  const featuredGame = games?.filter((game, index) => index < 3);
  const [index, setIndex] = React.useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect} className={car_wrapper}>
      {featuredGame?.map((game) => (
        <Carousel.Item key={game?.id} className={car_item}>
          <img
            className="d-block w-100"
            src={game?.cover?.includes('.jpg') ? `images/${game?.cover}` : 'images/cyberpunk.jpg'}
            alt={game?.name}
          />
          <Carousel.Caption className={car_caption}>
            <h3>{game?.name}</h3>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

export default HeroLanding;
