import React, { useState } from 'react';
import { Form, InputGroup, Button } from 'react-bootstrap';
import { BsSearch } from 'react-icons/bs';
import { search_wrapper, search_input, btn_search } from './Landing.module.css';

function Searchbar({ onSearch }) {
  const [keyword, setKeyword] = useState('');

  function onKeywordChangeHandler(event) {
    setKeyword(event.target.value);
  }

  function onSubmitHandler(event) {
    event.preventDefault();
    onSearch(keyword);
  }

  return (
    <>
      <Form onSubmit={onSubmitHandler}>
        <InputGroup className={search_wrapper}>
          <Form.Control
            className={search_input}
            placeholder="Search . . ."
            aria-label="Recipient's username"
            aria-describedby="basic-addon2"
            value={keyword}
            onChange={onKeywordChangeHandler}
          />
          <Button
            type="submit"
            variant="outline-secondary"
            id="button-addon2"
            className={btn_search}
          >
            <BsSearch />
          </Button>
        </InputGroup>
      </Form>
    </>
  );
}

export default Searchbar;
