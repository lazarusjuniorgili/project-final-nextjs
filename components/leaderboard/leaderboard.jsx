import React from "react";
import Navbar from "./navigation";
import Tabel from "./tabel";
import styles from "../../styles/leaderboard_style.module.css"
const LeaderBoard = () => {
  return (
    <div>
      <Navbar />
      <div className={styles.container_tabel}>
        <h1> LEADERBOARD</h1>
        <div className={styles.tabel}>
          <Tabel />
        </div>
      </div>
    </div>
  );
};

export default LeaderBoard;
