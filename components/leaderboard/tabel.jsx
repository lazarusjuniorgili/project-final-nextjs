import React from "react";
import Table from "react-bootstrap/Table";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import axios from "axios";

function StripedRowExample() {
  const [data, setData] = useState([]);
  const getData = async () => {
    try {
      const response = await axios.get(
        "http://localhost:5000/user"
      );
      setData(response.data.data);
    } catch (err) {}
  };
  useEffect(() => {
    getData();
  }, []);

  console.log(data)
  const sortData = data.sort((a, b) => b.total_point - a.total_point);

  return (
    <Table striped className="">
      <thead>
        <tr>
          <th className="w-25">RANK</th>
          <th className="w-25">USERNAME</th>
          <th className="w-25">NAME</th>
          <th className="w-25">SKOR</th>
        </tr>
      </thead>
      <tbody>
        {sortData.map((row, i) => {
          return (
            <tr key={i}>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {i + 1}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.username}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.name}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.total_point}
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}

export default StripedRowExample;
