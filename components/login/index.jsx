import axios from "axios";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Alert from "react-bootstrap/Alert";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Lottie from "lottie-react";
import anims from "../assets/24219-controller.json";
import styles from "../../styles/login_style.module.css";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const AuthRegister = () => {
    setTimeout(() => {
      router.replace({ pathname: "/register" });
    }, 1000);
  };

  const handleLogin = (event) => {
    event.preventDefault();
    const payload = { username, password };
    setLoading(true);

    try {
      axios
        .post("http://localhost:5000/login", payload)
        .then((res) => {
          localStorage.setItem("accessToken", res.data.data.accessToken);
          localStorage.setItem("userId", res.data.data.id);
          localStorage.setItem("username", res.data.data.username);
          setSuccess(res.data.message);
          setLoading(false);
          setTimeout(() => {
            setSuccess("");
            router.replace({ pathname: "/" });
          }, 3000);
        })
        .catch((err) => {
          setLoading(false);
          setError(
            err?.response?.data?.message
              ? err.response.data.message
              : err?.message
          );
          setTimeout(() => {
            setError("");
          }, 3000);
        });
    } catch {
      setLoading(false);
      setError("internal Server Error");
    }
  };

  return (
    <div className={styles.root}>
      <div className={styles.login_page}>
        <div className={styles.header_login}>
          <h1 className={styles.header}>PLAY MORE, FEEL LIVE MORE</h1>
        </div>
        <div className={styles.form_login}>
          <div className={styles.box_login}>
            <form className={styles.form}>
              <div className={styles.circle}>
                <Lottie animationData={anims}></Lottie>
              </div>
              <input
                placeholder="Username"
                className={styles.username_input}
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <input
                placeholder="Password"
                className={styles.password_input}
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Button
                variant="success"
                className={styles.login_button}
                onClick={handleLogin}
                type="submit"
                disabled={!username || !password}
              >
                LOGIN
              </Button>
              {success && (
                <Alert variant="success" className="text-center w-50 mx-auto">
                  {success}
                </Alert>
              )}
              {error && (
                <Alert variant="danger" className="text-center w-50 mx-auto">
                  {error}
                </Alert>
              )}
              {loading && (
                <Alert variant="light" className="text-center w-50 mx-auto">
                  Loading
                </Alert>
              )}
              <p className={styles.sign_up}>
                Need an account?
                <Button className={styles.btn_sign_up} onClick={AuthRegister}>
                  SIGN UP
                </Button>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
