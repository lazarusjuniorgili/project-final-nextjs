import React from "react";

function HistoryGameItem({ result, game, point, style }) {
  return (
    <div className={style.p_history_item}>
      <p>{game?.name}</p>
      <p>
        {result}
        <span> {point !== 0 ? `+${point}` : point}</span>
      </p>
    </div>
  );
}

export default HistoryGameItem;
