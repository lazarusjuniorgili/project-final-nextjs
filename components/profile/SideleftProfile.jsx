import React, { useState } from "react";
import { Col, Button, Modal, Form } from "react-bootstrap";

function SideleftProfile({ id, name, username, email, logout, onUpdate, style }) {
  const [nameInput, setNameInput] = useState(name);
  const [usernameInput, setUserameInput] = useState(username);
  const [emailInput, setEmailInput] = useState(email);
  const [showEdit, setShowEdit] = useState(false);

  const handleShowEdit = () => setShowEdit(true);
  const handleCloseEdit = () => setShowEdit(false);
  const onSubmitHandler = () => {
    handleCloseEdit();
    onUpdate(id, { name: nameInput, username: usernameInput, email: emailInput });
  };

  return (
    <Col sm={4} className={style.sideleft_container}>
      <div className={style.profile_item}>
        <img src={`https://ui-avatars.com/api/?name=${username}&background=random&size=200`} alt="profile avatar"></img>
      </div>
      <h2>{name}</h2>
      <p className={style.profile_name}>{username}</p>
      <p className={style.profile_name}>{email}</p>
      <Button variant="link" className={style.btn_edit} onClick={handleShowEdit}>
        Edit Data
      </Button>
      <Button variant="link" className={style.btn_logout} onClick={logout}>
        Logout
      </Button>

      <Modal show={showEdit} onHide={handleCloseEdit}>
        <Modal.Header closeButton className={style.modal_header}>
          <Modal.Title className={style.modal_title}>Edit Form</Modal.Title>
        </Modal.Header>
        <Modal.Body className={style.modal_body}>
          <h2>PLAY MORE, FEEL LIVE MORE</h2>
          <Form>
            <Form.Group className="mb-3">
              <Form.Control className={style.form_input} type="text" placeholder="name" value={nameInput} onChange={({ target }) => setNameInput(target.value)} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Control className={style.form_input} type="text" placeholder="username" value={usernameInput} onChange={({ target }) => setUserameInput(target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control className={style.form_input} type="email" placeholder="email" value={emailInput} onChange={({ target }) => setEmailInput(target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className={style.modal_footer}>
          <Button variant="secondary" className={style.btn_modal} onClick={handleCloseEdit}>
            Close
          </Button>
          <Button variant="primary" className={style.btn_modal} onClick={onSubmitHandler}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </Col>
  );
}

export default SideleftProfile;
