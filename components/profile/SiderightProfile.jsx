import React from "react";
import { Col } from "react-bootstrap";
import HistoryGameItem from "./HistoryGameItem";

function SiderightProfile({ total_point, history, style }) {
  return (
    <Col sm={8} className={style.sideright_container}>
      <h2 className={style.p_title_score}>Skor mu</h2>
      <p className={style.p_total_score}>{total_point ? total_point : 0}</p>
      <div className={style.p_container_history}>
        <h3 className={style.p_title_history}>HISTORY GAME</h3>
        {history?.map((data) => (
          <HistoryGameItem key={data?.id} {...data} style={style} />
        ))}
      </div>
    </Col>
  );
}

export default SiderightProfile;
