import { useState } from "react";
import axios from "axios";
import Alert from "react-bootstrap/Alert";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import { useRouter } from "next/router";
import styles from "../../styles/register_style.module.css";
import Lottie from "lottie-react";
import anims from "../assets/24219-controller.json";

const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const AuthLogin = () => {
    router.replace({ pathname: "/login" });
  };

  const handleRegister = (event) => {
    event.preventDefault();
    const payload = { username, name, password, email };
    setLoading(true);
    try {
      axios
        .post("http://localhost:5000/register", payload)
        .then((res) => {
          setSuccess(res.data.message);
          setLoading(false);
          setTimeout(() => {
            setSuccess("");
            router.replace({ pathname: "/login" });
          }, 3000);
        })
        .catch((err) => {
          setLoading(false);
          setError(
            err?.response?.data?.message
              ? err.response.data.message
              : err?.message
          );
          setTimeout(() => {
            setError("");
          }, 3000);
        });
    } catch {
      setLoading(false);
      setError("internal Server Error");
    }
  };

  return (
    <div className={styles.register_page}>
      <div className={styles.reg_container}>
        {success && (
          <Alert variant="success" className="text-center w-50 mx-auto">
            {success}
          </Alert>
        )}
        {error && (
          <Alert variant="danger" className="text-center w-50 mx-auto">
            {error}
          </Alert>
        )}
        {loading && (
          <Alert variant="light" className="text-center w-50 mx-auto">
            Loading
          </Alert>
        )}
        <div className={styles.box_register}>
          <form className={styles.form}>
            <div className={styles.circle}>
              {" "}
              <Lottie animationData={anims}></Lottie>
            </div>
            <input
              placeholder="Name"
              className={styles.name_input}
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <input
              placeholder="Username"
              className={styles.username_input}
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <div className={styles.input_email_pass}>
              <input
                placeholder="Email"
                className={styles.email_input}
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <input
                placeholder="Password"
                className={styles.password_input}
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <Button
              variant="light"
              className={styles.register_button}
              type="submit"
              onClick={handleRegister}
              disabled={!username || !password || !name || !email}
            >
              REGISTER
            </Button>
            <p className={styles.sign_in}>
              Already have an account?
              <Button className={styles.btn_sign_in} onClick={AuthLogin}>
                SIGN IN
              </Button>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
