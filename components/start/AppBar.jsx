import Link from "next/link";
import React from "react";
import { Navbar, Container } from "react-bootstrap";
import { HiHome } from "react-icons/hi2";

const AppBar = () => {
  return (
    <Navbar>
      <Container style={{ backgroundColor: "#65DBA3" }} className="px-4 shadow">
        <Navbar.Brand>
          <img
            alt=""
            src="/images/logo-game.png"
            height="50px"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Link href="/" className="nav-link">
          <HiHome size={"1.5rem"} />
        </Link>
      </Container>
    </Navbar>
  );
};

export default AppBar;
