import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Card, Col, Row, Alert, Button } from "react-bootstrap";
import CompChoice from "./CompChoice";
import UserChoice from "./UserChoice";

const Start = ({ startGame, round, pointGame, data }) => {
  const [show, setShow] = useState(false);
  const router = useRouter();
  const choices = ["batu", "gunting", "kertas"];
  const choiceStyle = {
    width: "150px",
    height: "100%",
  };
  const alertStyle = {
    backgroundColor: "#41B07B",
    color: "white",
    position: "absolute",
    zIndex: 10,
    maxWidth: "500px",
  };
  const headerStyle = { color: "#41B07B" };
  const vsStyle = { fontSize: "100px" };

  useEffect(() => {
    if (round === 3) {
      setTimeout(() => {
        setShow(true);
      }, 2000);
    }
  }, [round]);

  return (
    <Row className=" justify-content-center align-items-center">
      {show === true && (
        <Alert style={alertStyle}>
          <Alert.Heading>Ronde telah berakhir!!!</Alert.Heading>
          <p>
            {pointGame > 0
              ? `Selamat kamu mendapatkan ${pointGame} point`
              : "Yahhh kamu tidak mendapatkan point sama sekali"}
          </p>
          <hr />
          <div className="d-flex justify-content-end gap-1">
            <Button onClick={() => window.location.reload()} variant="warning">
              Main Lagi
            </Button>
            <Button onClick={() => router.push("/")} variant="danger">
              Sudah Cukup
            </Button>
          </div>
        </Alert>
      )}
      <Col>
        <UserChoice
          choices={choices}
          choiceStyle={choiceStyle}
          headerStyle={headerStyle}
          startGame={startGame}
          round={round}
        />
      </Col>
      <Col xs={2} className="d-grid align-items-center ">
        <Card.Text className="text-center fs-4 fw-bold text-danger text__info">
          {round === 3 ? "Round Selesai" : `Round ${round + 1}`}
        </Card.Text>
        <Card.Title
          style={vsStyle}
          className="text-center text-danger text__vs"
        >
          VS
        </Card.Title>
        <div>
          <Card.Text className="text-center fs-4 fw-bold text-danger text__info">
            {data?.result === undefined ? "Hasil: -" : `Hasil: ${data?.result}`}
          </Card.Text>
          <Card.Text className="text-center fs-4 fw-bold text-danger text__info">
            {`Total point: ${pointGame}`}
          </Card.Text>
        </div>
      </Col>
      <Col>
        <CompChoice
          choices={choices}
          choiceStyle={choiceStyle}
          headerStyle={headerStyle}
          comp={data?.comp}
        />
      </Col>
    </Row>
  );
};

export default Start;
