import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import {BsFillCaretLeftFill} from "react-icons/bs"
import AddGameInput from "../../components/add/AddGameInput";
import { addGame } from "../../utils/api";

const index = () => {
  const router = useRouter();

  const onAddGame = async (payload) => {
    await addGame(payload);

    router.push("/");
  }

  return (
    <Container>
      <Link
        href="/"
        style={{
          position: "absolute",
          color: "#1a8956",
          fontSize: "30px",
          left: "50px",
          top: "30px",
        }}
      >
        <BsFillCaretLeftFill />
      </Link>
      <Row style={{ height: "100vh" }}>
        <Col
          style={{ maxWidth: "800px" }}
          className=" d-grid align-items-center mx-auto"
        >
          <AddGameInput addGame={onAddGame} />
        </Col>
      </Row>
    </Container>
  );
};

export default index;
