import { Container } from "react-bootstrap";
import Detail from "../../../../components/game/Detail";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Game from "../../../../components/game/Game";
import { fetchData } from "../../../../utils/api";
import Appbar from "../../../../components/landing/Appbar";

const DetailGame = () => {
  const router = useRouter();
  const { id } = router.query;
  const [game, setGame] = useState(null);

  const getGuide = async () => {
    const response = await fetchData(id);

    setGame(response);
  };

  useEffect(() => {
    getGuide();
  }, []);
  return (
    <>
      <Appbar />
      <Container>
        <Game id={game?.id}>
          <Detail detail={game?.detail} />
        </Game>
      </Container>
    </>
  );
};

export default DetailGame;
