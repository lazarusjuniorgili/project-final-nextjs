import { Container } from "react-bootstrap";
import Guide from "../../../../components/game/Guide";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Game from "../../../../components/game/Game";
import { fetchData } from "../../../../utils/api";
import Appbar from "../../../../components/landing/Appbar";

const GuideGame = () => {
  const router = useRouter();
  const { id } = router.query;
  const [game, setGame] = useState(null);

  const getGuide = async () => {
    const response = await fetchData(id);

    setGame(response);
  };

  useEffect(() => {
    getGuide();
  }, []);

  return (
    <>
      <Appbar />
      <Container>
        <Game id={game?.id}>
          <Guide guide={game?.guide} />
        </Game>
      </Container>
    </>
  );
};

export default GuideGame;
