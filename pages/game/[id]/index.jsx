import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Game from "../../../components/game/Game";
import Hero from "../../../components/game/Hero";
import { fetchData } from "../../../utils/api";
import Appbar from "../../../components/landing/Appbar";

const GamePage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [game, setGame] = useState(null);

  const getId = async () => {
    const response = await fetchData(id);
    setGame(response);
  };

  useEffect(() => {
    getId();
  }, []);

  return (
    <>
      <Appbar />
      <Container>
        <Game id={game?.id}>
          <Hero id={game?.id} />
        </Game>
      </Container>
    </>
  );
};

export default GamePage;


