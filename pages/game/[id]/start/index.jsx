import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import AppBar from "../../../../components/start/AppBar";
import Start from "../../../../components/start/Start";
import { startGame } from "../../../../utils/api";

const StartGame = () => {
  const router = useRouter();
  const { id } = router.query;
  const [data, setData] = useState(null);
  const [round, setRound] = useState(0);
  const [pointGame, setPointGame] = useState(0);
  const [userId, setUserId] = useState(null);

  const increase = () => setRound((prevValue) => prevValue + 1);

  const onClickHandle = (choice) => {
    const payload = {
      user_id: parseInt(userId),
      game_id: parseInt(id),
      choice: choice,
    };

    startGame(payload).then((row) => {
      setData(row);
      setPointGame((prevValue) => prevValue + row.point);
    });

    increase();
  };

  useEffect(() => {
    const value = localStorage.getItem("userId");
    setUserId(value);
  }, []);

  console.log(userId)

  return (
    <>
      <AppBar />
      <Container>
        <Start
          startGame={onClickHandle}
          round={round}
          pointGame={pointGame}
          data={data}
        />
      </Container>
    </>
  );
};

export default StartGame;
