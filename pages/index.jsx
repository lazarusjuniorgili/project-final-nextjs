import axios from "axios";
import Header from "../components/landing/Header";
import Searchbar from "../components/landing/Searchbar";
import Appbar from "../components/landing/Appbar";
import HeroLanding from "../components/landing/HeroLanding";
import CardGame from "../components/landing/CardGame";
import Footer from "../components/landing/Footer";
import { Container } from "react-bootstrap";
import { useState } from "react";
import Error from "next/error";
import dataDummy from "../utils/games";
import BtnAdd from "../components/add/BtnAdd";

const LandingPage = ({ games }) => {
  const [keyword, setKeyword] = useState("");

  const onSearchHandler = (keyword) => setKeyword(keyword);

  const filteredGames = games?.filter((game) => {
    return game.name?.toLowerCase().includes(keyword.toLowerCase());
  });

  return (
    <>
      <Header />
      <Appbar />
      <main>
        <Container expand="lg">
          <Searchbar onSearch={onSearchHandler} />
          <HeroLanding games={games} />
          <CardGame games={filteredGames} />
        </Container>
        <BtnAdd />
      </main>
      <Footer />
    </>
  );
};

export default LandingPage;

export const getServerSideProps = async () => {
  const BASE_URL = "http://localhost:5000";

  let games;

  try {
    const response = await axios.get(`${BASE_URL}/game`);
    games = response.data.data;
    if (games.length === 0) {
      games = dataDummy;
    }
  } catch (err) {
    console.log(err);
    games = dataDummy;
    // games = { error: true };
  }

  return {
    props: {
      games,
    },
  };
};
