/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import axios from "axios";
import { Container, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import { BsFillCaretLeftFill } from "react-icons/bs";
import SideleftProfile from "../../components/profile/SideleftProfile";
import SiderightProfile from "../../components/profile/SiderightProfile";
import style from "../../styles/profile.module.css";
import Link from "next/link";

function ProfilePage() {
  const router = useRouter();

  const [profile, setProfile] = useState(null);
  const [history, setHistory] = useState(null);

  const URL = "http://localhost:5000";

  async function getUserLogged() {
    const token = window.localStorage.getItem("accessToken");
    try {
      const response = await axios.get(`${URL}/whoami`, {
        headers: {
          Authorization: token,
        },
      });
      return { error: false, data: response.data };
    } catch (err) {
      if (err.code === "ERR_NETWORK") return { error: true, message: "Gagal konek ke server" };
      return { error: true, message: err.message };
    }
  }

  async function getAllHistory() {
    try {
      const response = await axios.get(`${URL}/history`);
      return { error: false, message: response.data.message, data: response.data.data };
    } catch (err) {
      if (err.code === "ERR_NETWORK") return { error: true, message: "Gagal konek ke server" };
      return { error: true, message: err.message };
    }
  }

  async function putUserLogged(id, user) {
    const token = window.localStorage.getItem("accessToken");
    try {
      const response = await axios.put(`${URL}/user/${id}`, user, {
        headers: {
          Authorization: token,
        },
      });
      return { error: false, message: response.data.message, data: response.data.data };
    } catch (err) {
      if (err.code === "ERR_NETWORK") return { error: true, message: "Gagal konek ke server" };
      return { error: true, message: err.response.data.message, data: null };
    }
  }

  async function onUpdateUserHandler(id, user) {
    const { error, message, data } = await putUserLogged(id, user);
    if (error) return alert(message);
    setProfile(data);
  }

  function onLogoutHandler() {
    localStorage.clear();
    router.push("/");
  }

  useEffect(() => {
    getUserLogged().then(({ error, message, data }) => {
      if (error) return alert(message);
      setProfile(data);
    });
    getAllHistory().then(({ error, data }) => {
      if (error) return;
      setHistory(data);
    });
  }, []);

  const sortHistory = history?.slice().sort((a, b) => b.id - a.id);

  return (
    <Container fluid>
      <Row>
        <Link href="/" className={style.btn_back}>
          <BsFillCaretLeftFill />
        </Link>
        {profile && <SideleftProfile {...profile} logout={onLogoutHandler} onUpdate={onUpdateUserHandler} style={style} />}
        {history && <SiderightProfile {...profile} history={sortHistory} style={style} />}
      </Row>
    </Container>
  );
}

export default ProfilePage;
