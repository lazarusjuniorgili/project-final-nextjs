import axios from "axios";

const BASE_URL = "http://localhost:5000/game";

export const fetchData = async (id) => {
  const res = await axios.get(`${BASE_URL}/${id}`);

  return res.data.data;
};

export const addGame = async (payload) => {
  const res = await axios.post(`${BASE_URL}/`, payload);
  
  return res;
}

export const startGame = async (payload) => {
  const res = await axios.post(`${BASE_URL}/startgame`, payload);

  return res.data;
}